
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.ForkJoinPool;
import java.util.Arrays;

public class Sort {

public static void main(String[] args) {
	final int SIZE = 7000000;
	Integer[] list1 = new Integer[SIZE];
	Integer[] list2 = new Integer[SIZE];

	for (int i = 0; i < list1.length; i++)
	list1[i] = list2[i] = (int) (Math.random() * 10000000);

	long startTime = System.currentTimeMillis();
	parallelMergeSort(list1);
	long endTime = System.currentTimeMillis();
	System.out.println("Parallel merge sort time is " + (endTime - startTime) + " milliseconds.");

	long startTime2 = System.currentTimeMillis();
	MergeSort.mergeSort(list2);
	long endTime2 = System.currentTimeMillis();
	System.out.println("\nSequential merge sort time is " + (endTime2 - startTime2) + " milliseconds.");

	}

public static <E extends Comparable<E>> void parallelMergeSort(E[] list) {
	RecursiveAction mainTask = new SortTask<E>(list);
	ForkJoinPool pool = new ForkJoinPool();
	pool.invoke(mainTask);

	}

private static class SortTask<E extends Comparable<E>> extends RecursiveAction {
	private final int THRESHOLD = 500;
	private E[] list;

SortTask(E[] list) {
	this.list = list;

	}

@Override
protected void compute() {
	if (list.length < THRESHOLD)
	Arrays.sort(list);
	else {
	E[] firstHalf = (E[])(new Comparable[list.length / 2]);
	System.arraycopy(list, 0, firstHalf, 0, list.length / 2);

	int secondHalfLength = list.length - list.length / 2;
	E[] secondHalf = (E[])(new Comparable[secondHalfLength]);
	System.arraycopy(list, list.length / 2, secondHalf, 0, secondHalfLength);

	invokeAll(new SortTask<E>(firstHalf), new SortTask<E>(secondHalf));
	MergeSort.merge(firstHalf, secondHalf, list);

	}
	}
	}

public static class MergeSort  {

public static <E extends Comparable<E>> void mergeSort(E[] list) {
	if (list.length > 1) {
	E[] firstHalf = (E[])(new Comparable[list.length / 2]);
	System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
	mergeSort(firstHalf);

	int secondHalfLength = list.length - list.length / 2;
	E[] secondHalf = (E[])(new Comparable[secondHalfLength]);
	System.arraycopy(list, list.length / 2, secondHalf, 0, secondHalfLength);
	mergeSort(secondHalf);

	merge(firstHalf, secondHalf, list);

	}
	}

public static <E extends Comparable<E>> void merge(E[] list1, E[] list2, E[] temp) {
	int current1 = 0;
	int current2 = 0;
	int current3 = 0;

	while (current1 < list1.length && current2 < list2.length) {
	if (list1[current1].compareTo(list2[current2]) < 0)
	temp[current3++] = list1[current1++];
	else
	temp[current3++] = list2[current2++];

	}

	while (current1 < list1.length)
	temp[current3++] = list1[current1++];

	while (current2 < list2.length)
	temp[current3++] = list2[current2++];

	}
	}

}
